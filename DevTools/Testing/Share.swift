//
//  Share.swift
//  Testing
//
//  Created by lydio on 10/2/21.
//

import Foundation

class Share {
    func share(localFilePath:String) {}
}

class Dropbox: Share {
    
    var bearerToken = "7ik2sLkowokAAAAAAAAAAdazgtZc4IeRylmU2uzrgnd4LEEGKGoeWfBNE4hqLg6_"
   
    override func share(localFilePath:String) {
        
        let remoteFilePath = remoteFileName(path: localFilePath) //without first '/'
        
        let request = "curl -X POST https://content.dropboxapi.com/2/files/upload"
        let authHeader = "--header \"Authorization: Bearer \(bearerToken)\""
        let argsHeader = "--header \"Dropbox-API-Arg: {\\\"path\\\": \\\"/\(remoteFilePath)\\\"}\""
        let contentTypeHeader = "--header \"Content-Type: application/octet-stream\""
        let data = "--data-binary @\(localFilePath)"
        let command = [request, authHeader, argsHeader, contentTypeHeader, data].joined(separator: " ")
        
        let log = Utils.shell(command)
        print(log)
    }
}

class GoogleDrive:Share {
    
    var clientId = "856061290035-klb06bqcsqnsnp64ob48qjq5ohg6vbre.apps.googleusercontent.com"
    var clientSecret = "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"
    var deviceCode = "AH-1Ng1JvaK2cCE046Q9eR0K4M7IA1MLaf0O721jxgAk09rRoPNLZhNvuMaQJaXD8O2e2gh_cOhhbPnv85H_MQAT98ReRc1mpQ"
    var userCode = "LCH-ZJJ-KLS"
    var accessToken = "ya29.a0ARrdaM8rVd2zgCv0jSbRbvrtUf0dyxB-yrdmtDtprwCl71_Y4Ba-T8fj_Fr99uyuZzRNgLOYv5OA_gYwFOSOLCwT6CZ9FeTxJEIP1C9YKaND_ie9Ib2F9h0Cl1MyXKhew4TwUT47SUjee2Pk6ty0AxWhkIgi"
    var refreshToken = "1//09xgtMtLBWBbKCgYIARAAGAkSNwF-L9Irc0UMbxo9uqh9l5LGdKQrpwO9R-MtdIhQtai2S-yY3FXJFE72vYA2l73f3GzWIhkjHEw"
   
    override func share(localFilePath: String) {
        
        //let log = Utils.shell("curl -d \"client_id=\(clientId)&scope=https://www.googleapis.com/auth/drive.file\" https://oauth2.googleapis.com/device/code")
        //print(log)
        
        //let log = Utils.shell("curl -d client_id=\(clientId) -d client_secret=\(clientSecret) -d device_code=\(deviceCode) -d grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Adevice_code https://accounts.google.com/o/oauth2/token")
        //print(log)
        
        var request = "curl -X POST https://accounts.google.com/o/oauth2/token"
        let data = "--data \"client_id=\(clientId)&client_secret=\(clientSecret)&refresh_token=\(refreshToken)&grant_type=refresh_token\""
        var command = [request, data].joined(separator: " ")
        
        var log = Utils.shell(command)
        print(log)
        
        Utils.parseJson(from: log, recursive: false) { (key, value) in
            if key == "access_token", let value = value as? String {
                accessToken = value
                return true
            }
            return false
        }
        
        let remoteFileName = remoteFileName(path: localFilePath)
        //let _ = Utils.shell("zip \(localFilePath).zip \(localFilePath)")
        
        request = "curl -X POST -L https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart"
        let authHeader = "-H \"Authorization: Bearer \(accessToken)\""
        let metadata = "-F \"metadata={\\\"name\\\":\\\"\(remoteFileName)\\\"};type=application/json;charset=UTF-8\""
        //let file = "-F \"file=@\(localFilePath).zip;type=application/zip\""
        let file = "-F \"file=@\(localFilePath);type=application/octet-stream\""
        command = [request, authHeader, metadata, file].joined(separator: " ")
        
        log = Utils.shell(command)
        print(log)
    }
}

class GMail:Share {
    
    var emailTo = "pd2devtools@gmail.com"
    var emailFrom = "pd2devtools@gmail.com"
    var password = "pDBMd2Rm"
    
    override func share(localFilePath: String) {
        
        let request = "curl -n --ssl-reqd --url smtps://smtp.gmail.com:465"
        let rcpt = "--mail-rcpt \"\(emailTo)\""
        let from = "--mail-from \"\(emailFrom)\""
        let user = "--user \(emailFrom):\(password) "
        let file = "--upload-file \(localFilePath)"
        
        let command = [request, rcpt, from, user, file].joined(separator: " ")
        
        let log = Utils.shell(command)
        print(log)
    }
}

private func remoteFileName(path:String) -> String {
    
    let timestamp = String(Int(Date().timeIntervalSince1970))
    var components = path.split(separator: "/")
    return "\(timestamp)-\(components.removeLast())"
    
}
