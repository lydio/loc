//
//  BuildResult.swift
//  Testing
//
//  Created by lydio on 10/1/21.
//

import Foundation

struct Result:Encodable {
    
    var startedTime:String?
    var endedTime:String?
    var buildResult:BuildResult?
    var actionResult:ActionResult?
    
    init(dict:[String:Any]) {
        startedTime = value(from: dict, forKey: "startedTime")
        endedTime = value(from: dict, forKey: "endedTime")
        //print(startedTime, endedTime)
        
        if let dict = dict["buildResult"] as? [String:Any] {
            buildResult = BuildResult(dict: dict)
        }
        if let dict = dict["actionResult"] as? [String:Any] {
            actionResult = ActionResult(dict: dict)
        }
    }
}

struct BuildResult:Encodable {
    
    var status:String?
    var logRef:Ref?
    
    init(dict:[String:Any]) {
        status = value(from: dict, forKey: "status")
        //print(status)
        
        if let dict = dict["logRef"] as? [String:Any] {
            logRef = Ref(dict: dict)
        }
    }
}

struct ActionResult:Encodable {
    
    var status:String?
    var logRef:Ref?
    var testsRef:Ref?
    var testsCount:String?
    var testFailureSummaries:[TestFailureIssueSummary]?
    
    init(dict:[String:Any]) {
        status = value(from: dict, forKey: "status")
        //print(status)
        
        if let dict = dict["logRef"] as? [String:Any] {
            logRef = Ref(dict: dict)
        }
        if let dict = dict["testsRef"] as? [String:Any] {
            testsRef = Ref(dict: dict)
        }
        if let dict = dict["issues"] as? [String:Any] {
            let arr:[[String:Any]]? = array(from: dict, forKey: "testFailureSummaries")
            testFailureSummaries = arr?.map { TestFailureIssueSummary(dict: $0) }
            //print(testFailureSummaries)
        }
        if let dict = dict["metrics"] as? [String:Any] {
            testsCount = value(from: dict, forKey: "testsCount")
        }
    }
}

struct TestFailureIssueSummary:Encodable {
    
    var producingTarget:String?
    var testCaseName:String?
    var message:String?
    var issueType:String?
    var url:String?
    
    init(dict:[String:Any]) {
        producingTarget = value(from: dict, forKey: "producingTarget")
        testCaseName = value(from: dict, forKey: "testCaseName")
        message = value(from: dict, forKey: "message")
        issueType = value(from: dict, forKey: "issueType")
        //print(producingTarget, testCaseName, message, issueType)
        
        if let dict = dict["documentLocationInCreatingWorkspace"] as? [String:Any] {
            url = value(from: dict, forKey: "url")
            //print(url)
        }
    }
}

struct Ref:Encodable {

    var id:String?
    
    init(dict:[String:Any]) {
        id = value(from: dict, forKey: "id")
        //print(id)
    }
}

private func value<T>(from dict:[String:Any]?, forKey key:String) -> T? {
    guard let dict = dict else {
        return nil
    }
    return (dict[key] as? [String:Any])?["_value"] as? T
}

private func array<T>(from dict:[String:Any]?, forKey key:String) -> [T]? {
    guard let dict = dict else {
        return nil
    }
    return (dict[key] as? [String:Any])?["_values"] as? [T]
}

extension Result:CustomStringConvertible {
    
    var description: String {
        
        let rowTmpl = "%@: %@\n"
        let printable = printable()
        let infoTable = printable.infoTable.map { String(format: rowTmpl, $0.0, $0.1) }.joined()
        let failureTables = printable.failureTables.map { $0.map { String(format: rowTmpl, $0.0, $0.1) }.joined() }
        return "\n\(infoTable)\n\(failureTables.joined(separator:"\n"))"
    }
    
    var html:String {
        
        let rowTmpl = "<tr><td align=\"right\" style=\"padding-right:0.4em;\">%@</td><td>%@</td></tr>"
        let printable = printable()
        let infoTable = printable.infoTable.map { String(format: rowTmpl, $0.0, $0.1) }.joined()
        let failureTables = printable.failureTables.map { $0.map { String(format: rowTmpl, $0.0, $0.1) }.joined() }
        return "<html><body><table>\(infoTable)</table><br>\(failureTables.map {"<table>\($0)</table>" }.joined(separator: "<br>"))</body></html>"
    }
    
    private func printable() -> (infoTable: [(String, String)], failureTables: [[(String, String)]]) {
        
        typealias Printable = (String?)->(String)
        let text:Printable = { $0 ?? "-" }
        let className:Printable = { String($0?.split(separator: ".").first ?? "-") }
        let methodName:Printable = { String($0?.split(separator: ".").last ?? "-") }
        let lineNumber:Printable = { String($0?.split(separator: "&").last?.split(separator: "=").last ?? "-") }
        let dateTime:Printable = { $0?.split(separator: ".").first?.replacingOccurrences(of: "T", with: " ") ?? ""}
        let testsCount = " (\(actionResult?.testFailureSummaries?.count ?? 0)/\(actionResult?.testsCount ?? "0"))"
        
        let infoTable = [
            ("Started Time",  dateTime(startedTime)),
            ("Ended Time", dateTime(endedTime)),
            ("Build Status", text(buildResult?.status)),
            ("Test Status",  text(actionResult?.status) + testsCount)
        ]
        let failureTables = actionResult?.testFailureSummaries?.map {
            [
                ("Target", text($0.producingTarget)),
                ("Test Class", className($0.testCaseName)),
                ("Test Method", methodName($0.testCaseName)),
                ("Line Number", lineNumber($0.url)),
                ("Issue Type", text($0.issueType)),
                ("Message", text($0.message))
            ]
        }
        return (infoTable, failureTables ?? [])
    }
    
}
