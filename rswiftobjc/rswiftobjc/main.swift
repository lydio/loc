//
//  main.swift
//  rswiftobjc
//
//  Created by lydio on 06.03.2021.
//

import Foundation

var sourceFilePath = "R.generated.swift"
var destinationFilePath = "RObjc.generated.swift"

let argCount = CommandLine.argc
if argCount == 3 {
    sourceFilePath = CommandLine.arguments[1]
    destinationFilePath = CommandLine.arguments[2]
}


if var text = try? String(contentsOf: URL(fileURLWithPath: sourceFilePath)) {
    let robjc = RObjc.create(fromText: text)
    text = """
    import Foundation
    import Rswift
    import UIKit
    \n
    """
    robjc.first?.toText(&text)
    try? text.write(toFile: destinationFilePath, atomically: true, encoding: String.Encoding.utf8)
    //print(text)
}

class RObjc {
    
    let structName:String
    private var funcNames:[String] = []
    private var blockText:String = ""
    private var nested:[RObjc] = []
    
    var className:String {
        let roots = ["R"]
        return roots.contains(structName) ? "RObjc" : "RObjc" + structName.firstUppercased()
    }
    var varName:String {
        let roots = ["R", "image", "file", "storyboard", "color", "reuseIdentifier", "string"]
        return roots.contains(structName) ? structName : structName.firstLowercased()
    }
    
    init(structName: String) {
        self.structName = structName
    }
    
    class func create(fromText text: String) -> [RObjc] {
        
        var robjc = [RObjc]()
        
        let comment = CommentCollector()
        let block = BlockCollector()
        let subBlock = BlockCollector()
        let structName = StructNameCollector()
        let funcName = FuncNameCollector()
        
        var i0 = 0
        var i = 0
        for ch in text {
            i += 1
            if comment.collect(ch) {
                continue
            }
            
            var sName = ""
            
            switch block.collect(ch) {
                case .waitingForOpen:
                    sName = structName.collect(ch)
                    break
                case .switchingToOpen:
                    sName = structName.collect(ch)
                    i0 = i
                    break
                case .waitingForClose:
                    if subBlock.collect(ch) == .waitingForOpen {
                        let fName = finilizeFuncName(funcName.collect(ch))
                        if fName.count > 0 {
                            robjc.last?.funcNames.append(fName)
                        }
                    }
                    break
                case .switchingToClose:
                    if i0 < (i - 1) {
                        robjc.last?.blockText = text[i0..<(i - 1)]
                    }
                    break
            }
            
            if sName.count > 0 {
                robjc.append(RObjc(structName: sName))
            }
        }
        let excludeStruct = ["R_", "intern", ""]
        robjc = robjc.filter { !excludeStruct.contains($0.structName) }
        for r in robjc {
            if r.blockText.count > 0 {
                r.nested = create(fromText: r.blockText)
            }
        }
        
        return robjc
    }
    
    private class func finilizeFuncName(_ fullName: String) -> String {
        
        guard let i0 = fullName.firstIndex(of: "(") else {
            return ""
        }
        let shortName = String(fullName[..<i0])
        let excludeFunc = ["localeBundle", "infoPlistString", "validate", "accentColor", ""]
        if excludeFunc.contains(shortName) {
            return ""
        }
        
        var strArgs = String(fullName[i0...])
        if strArgs == "()" {
            return fullName
        }
        strArgs = String(strArgs[1..<(strArgs.count - 1)])
        
        strArgs = strArgs.split(separator: ",")
            .map{$0.trimmingCharacters(in: .whitespacesAndNewlines)}
            .filter { $0.hasPrefix("_ value")}
            .joined(separator: ", ")
        
        return shortName + "(" + strArgs + ")"
    }
    
    func toText(_ t: inout String, path:String = ""){
        
        var path = path
        var def = " "
        
        if path.count > 0 {
            path += "."
        } else {
            def = " static "
        }
        path += varName
        
        t += ("@objc class " + className + ": NSObject {\n")
        
        for n in nested {
            t += "    @objc" + def + "let " + n.varName + " = " + n.className + "()\n"
        }
        
        for f in funcNames {
            if f.hasSuffix("()") {
                t += "    @objc let " + f.dropLast(2) + " = " + path + "." + f + "\n"
            } else {
                t += "    @objc func " + f + " -> String {\n"
                t += "        return " + path + "." + callFunc(f) + "\n"
                t += "    }\n"
            }
        }
        
        t += "}\n"
        
        for n in nested {
            n.toText(&t, path: path)
        }
    }
    
    private func callFunc(_ fullName: String) -> String {
        
        guard let i0 = fullName.firstIndex(of: "(") else {
            return ""
        }
        var call = String(fullName[...i0])
        let strArgs = String(fullName[i0...])
        let count = strArgs.split(separator: ",").count
        
        for i in 1...count {
            if i > 1 {
                call += ", "
            }
            call += "value\(i)"
        }
        call += ")"
        
        return call
    }
}

private class CommentCollector {
    
    private var slachesCount = 0
    
    func collect(_ ch: Character) -> Bool {
        
        if ch == "/" {
            slachesCount += 1
        } else if ch.isNewline || slachesCount == 1 {
            slachesCount = 0
        }
        return slachesCount > 1
    }
}

private class BlockCollector {
    
    enum State {
        case waitingForOpen, waitingForClose, switchingToOpen, switchingToClose
    }
    
    private var openBraceCount = 0
    private var closedBraceCount = 0
    
    func collect(_ ch: Character) -> State {
        
        if ch == "{" {
            openBraceCount += 1
            if openBraceCount == 1 && closedBraceCount == 0 {
                return .switchingToOpen
            }
        } else if ch == "}" {
            closedBraceCount += 1
            
            if openBraceCount > 0 && openBraceCount == closedBraceCount {
                openBraceCount = 0
                closedBraceCount = 0
                return .switchingToClose
            }
        }
        
        return openBraceCount > closedBraceCount ? .waitingForClose : .waitingForOpen
    }
}

private class StructNameCollector {
    
    private let def = "struct"
    private var queue = ""
    
    func collect(_ ch: Character) -> String {
        
        queue = queue + String(ch)
        
        if queue.hasPrefix(def) {
            if ch == ":" || ch == "{" {
                let name = queue[def.count..<(queue.count - 1)].trimmingCharacters(in: .whitespaces)
                queue = ""
                return name
            }
        } else {
            if queue.count > def.count {
                queue = String(queue.dropFirst(1))
            }
        }
        return ""
    }
}

private class FuncNameCollector {
    
    private let def = "static func"
    private var queue = ""
    
    private var openBracketCount = 0
    private var closedBracketCount = 0
    
    func collect(_ ch: Character) -> String {
        
        queue = queue + String(ch)
        
        if queue.hasPrefix(def) {
            if ch == "(" {
                openBracketCount += 1
            } else if ch == ")" {
                closedBracketCount += 1
                if openBracketCount > 0 && openBracketCount == closedBracketCount {
                    let name = queue[def.count..<queue.count].trimmingCharacters(in: .whitespaces)
                    openBracketCount = 0
                    closedBracketCount = 0
                    queue = ""
                    return name
                }
            }
        } else {
            if queue.count > def.count {
                queue = String(queue.dropFirst(1))
            }
        }
        return ""
    }
}

private extension String {
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start ..< end])
    }
    
    subscript (r: CountableClosedRange<Int>) -> String {
        let startIndex =  self.index(self.startIndex, offsetBy: r.lowerBound)
        let endIndex = self.index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        return String(self[startIndex...endIndex])
    }
    
    func firstUppercased() -> String {
        return count > 0 ? prefix(1).uppercased() + dropFirst() : ""
    }
    
    func firstLowercased() -> String {
        return count > 0 ? prefix(1).lowercased() + dropFirst() : ""
    }
}

