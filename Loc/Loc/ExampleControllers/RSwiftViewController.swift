//
//  RSwiftViewController.swift
//  Loc
//
//  Created by lydio on 04.03.2021.
//

import UIKit


class RSwiftViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = R.string.locExample.rSwiftTitle()
    }
}

extension RSwiftViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        if Int.random(in: 1...1000) < 500 {
            cell.textLabel?.text = R.string.locOthers.key1()
            cell.imageView?.image = R.image.value1()
        } else {
            cell.textLabel?.text = R.string.locOthers.key1000()
            cell.imageView?.image = R.image.value1000()
        }
        
        return cell
    }
}

