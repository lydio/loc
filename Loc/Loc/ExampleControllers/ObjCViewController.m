//
//  ObjCViewController.m
//  Loc
//
//  Created by lydio on 05.03.2021.
//

#import "ObjCViewController.h"
#import <Loc-Swift.h>


@interface ObjCViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@end

@implementation ObjCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = RObjc.string.locExample.rSwiftTitle;
    
    [(UIButton *)[self.view viewWithTag:10] setTitle: RObjc.string.locControls.createButton forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:11] setTitle: RObjc.string.locControls.editButton forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:12] setTitle:  RObjc.string.locControls.deleteButton forState:UIControlStateNormal];
    
    [(UIImageView *)[self.view viewWithTag:20] setImage:RObjc.image.mmJpg];
    
    NSString *hello = [RObjc.string.locControls helloLabel: @"Objective-C"]; //RObjc.string.locExample.message
    [(UILabel *)[self.view viewWithTag:30] setText: hello];
    
    [self valueChanged:self.slider];
}

- (IBAction)valueChanged:(UISlider *)slider {
    
    float value = slider.value * 100;
    NSString *units = value > 0 ? @"UAH" : @"";
    
    self.label.text = [RObjc.string.locControls totalLabel:value :units];
}

@end
