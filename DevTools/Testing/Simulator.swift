//
//  Simulator.swift
//  Testing
//
//  Created by lydio on 10/3/21.
//

import Foundation


class Simulator:Codable {
    
    private(set) var deviceTypeId:String = ""
    private(set) var runtimeId:String = ""
    private(set) var deviceName:String = ""
    private(set) var numberOfSimulators = 0
    private(set) var orientation:Bool = true
    private(set) var udid:String = ""
    
    func setup() {
        
        let _ = Utils.shell("xcrun simctl delete unavailable")
        
        let text = Utils.shell("xcrun simctl list -j")
        Utils.parseJson(from: text, recursive: true) { [unowned self] (key, value) in
            if key == "devicetypes" , let arr = value as? [[String:Any]] {
                let items = arr.compactMap { ($0["name"], $0["identifier"]) as? (String, String) }
                    .filter { $0.0.contains("iPhone") || $0.0.contains("iPad") }
                if let index = Utils.selectItem(items: items.map { $0.0 }, name: "device")?.index {
                    self.deviceTypeId = items[index].1
                    self.deviceName = items[index].0
                }
                return true
            }
            return false
        }
        
        Utils.parseJson(from: text, recursive: true) { [unowned self] (key, value) in
            if key == "runtimes" , let arr = value as? [[String:Any]] {
                let items = arr.compactMap { ($0["name"], $0["identifier"]) as? (String, String) }
                    .filter { $0.0.contains("iOS") }
                if let index = Utils.selectItem(items: items.map { $0.0 }, name: "iOS version")?.index {
                    self.runtimeId = items[index].1
                    self.deviceName += (", " + items[index].0)
                }
                return true
            }
            return false
        }
        
        print("📘 Set portrait or landscape orientation (P/L):")
        if let input = readLine(), input.uppercased() == "L" {
            orientation = false
        } else {
            orientation = true
        }
        
        print("📘 Set number of simulators (1...8):")
        if let number = readLine(), let n = Int(number), n >= 1, n <= 8 {
            numberOfSimulators = n
        }
    }
    
  
    func prepareForTesting() {
        
        let _ = Utils.shell("xcrun simctl shutdown all")
        
        let text = Utils.shell("xcrun simctl list devices -j")
        Utils.parseJson(from: text, recursive: true) { [unowned self] (key, value) in
            if let dict = value as? [String:[[String:Any]] ] {
                for arr in dict.values {
                    var items = arr.compactMap { ($0["name"], $0["udid"]) as? (String, String) }
                        .filter { $0.0.contains(self.deviceName) }
                    if let _ = items.first {
                        self.udid = items.removeFirst().1
                        items.forEach { let _ =  Utils.shell("xcrun simctl delete \($0.1)")}
                    }
                }
            }
            return true
        }
        
        if udid.isEmpty {
            udid = Utils.shell("xcrun simctl create \"\(deviceName)\" \(deviceTypeId) \(runtimeId)")
        }
        fixPreferences(udid: udid)
        let _ = Utils.shell("xcrun simctl boot \(udid); xcrun simctl bootstatus \(udid) && sleep 2")
        let _ = Utils.shell("open -a Simulator --args -CurrentDeviceUDID \(udid)")
    }
    
    private func fixPreferences(udid:String) {
      
        let path = "$HOME/Library/Preferences/com.apple.iphonesimulator.plist"
        let command = "/usr/libexec/PlistBuddy -c"
        
        var value = "true"
        var set = "\"Set :DevicePreferences:\(udid):ConnectHardwareKeyboard \(value)\" \"\(path)\""
        var add = "\"Add :DevicePreferences:\(udid):ConnectHardwareKeyboard bool \(value)\" \"\(path)\""
        var log = Utils.shell((command + " " + set) + " || " + (command + " " + add))
        //print(log)
        
        value = orientation ? "Portrait" : "LandscapeLeft"
        set = "\"Set :DevicePreferences:\(udid):SimulatorWindowOrientation \(value)\" \"\(path)\""
        add = "\"Add :DevicePreferences:\(udid):SimulatorWindowOrientation string \(value)\" \"\(path)\""
        log = Utils.shell((command + " " + set) + " || " + (command + " " + add))
        //print(log)
    }
}
