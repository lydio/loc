//
//  Utils.swift
//  DevTools
//
//  Created by lydio on 9/29/21.
//

import Foundation

enum Utils {
    
    static func shell(_ command: String) -> String {
        
        let task = Process()
        task.launchPath = "/bin/bash"
        task.arguments = ["-c", command]

        let pipe = Pipe()
        task.standardOutput = pipe
        task.launch()

        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output: String = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String

        return output.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    static func parseJson(from text:String, recursive:Bool, stop:(_ key:String, _ value:Any)->(Bool))  {
        
        var json:Any?
        if let data = text.data(using: .utf8) {
            do {
                json = try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        
        func parse(json:Any?) {
            if let arr = json as? [Any] {
                for i in 0..<arr.count {
                    if stop(String(i), arr[i]) {
                        break
                    }
                    if recursive {
                        parse(json: arr[i])
                    }
                }
            }
            if let dict = json as? [String: Any] {
                for k in dict.keys {
                    if stop(k, dict[k]!) {
                        break
                    }
                    if recursive {
                        parse(json: dict[k])
                    }
                }
            }
        }
        return parse(json: json)
    }
    
    static func findContents(at path: String, recursive:Bool, stop:(_ path:String, _ name: String, _ ext:String, _ isDir:Bool)->(Bool)) {
        
        do {
            let url = URL(fileURLWithPath: path)
            let urls = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: [.isDirectoryKey, .isRegularFileKey], options: [])
            for u in urls {
                let attr = try u.resourceValues(forKeys:[.isDirectoryKey, .isRegularFileKey])
                if attr.isDirectory == true {
                    if stop(u.path, u.lastPathComponent, u.pathExtension, true)  {
                        break
                    }
                    if recursive {
                        findContents(at: u.path, recursive: recursive, stop: stop)
                    }
                }
                if attr.isRegularFile == true {
                    if stop(u.path, u.lastPathComponent, u.pathExtension, false) {
                        break
                    }
                }
            }
        } catch let error {
            print(error)
        }
    }
    
    static func readFromJsonFile<T:Decodable>(path:String) -> T? {
        if let data = FileManager.default.contents(atPath: path), let app = try? JSONDecoder().decode(T.self, from: data) {
            return app
        }
        return nil
    }
    
    static func writeToJsonFile<T:Encodable>(_ t:T, path:String) {
        let url = URL(fileURLWithPath: path)
        try? FileManager.default.createDirectory(at: url.deletingLastPathComponent(), withIntermediateDirectories: true, attributes: nil)
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        do {
            let data = try encoder.encode(t)
            try data.write(to: url)
        } catch {
            print(error)
        }
    }
    
    static func readFromTextFile(path:String) -> String? {
        let url = URL(fileURLWithPath: path)
        do {
            return try String(contentsOf: url, encoding: .utf8)
        } catch {
            print(error)
        }
        return nil
    }
    
    static func writeTextFile(_ text:String, path:String) {
        let url = URL(fileURLWithPath: path)
        try? FileManager.default.createDirectory(at: url.deletingLastPathComponent(), withIntermediateDirectories: true, attributes: nil)
        do {
            try text.write(to: url, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print(error)
        }
    }
    
    static func selectItem(items:[String], name:String? = nil) -> (value:String, index:Int)? {
        
        if !items.isEmpty {
            for i in 1...items.count {
                print("\(i)) \(items[i - 1])")
            }
            
            let name = name ?? "item"
            let range = items.count > 1 ? "1...\(items.count)" : "1"
            print("📘 Select \(name) from list by index (\(range)):")
            
            if let index = readLine(), let i = Int(index), i > 0, i <= items.count {
                return (items[i - 1], i - 1)
            }
        }
        return nil
    }
    
    static func createDirectory(at path: String) -> Bool {
        let url = URL(fileURLWithPath: path)
        do {
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error)
            return false
        }
        return true
    }
    
    static func removeItem(at path: String) -> Bool {
        let url = URL(fileURLWithPath: path)
        do {
            let attr = try url.resourceValues(forKeys:[.isDirectoryKey])
            if attr.isDirectory == true {
                let urls = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: [.isDirectoryKey], options: [])
                for u in urls {
                    if !removeItem(at: u.path) {
                        throw NSError()
                    }
                }
            }
            try FileManager.default.removeItem(at: url)
        } catch _ {
            return false
        }
        return true
    }

    //📕: error message
    //📙: warning message
    //📗: ok status message
    //📘: action message
    //📓: canceled status message
    //📔: Or anything you like and want to recognize immediately by color
}
