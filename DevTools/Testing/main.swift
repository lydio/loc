//
//  main.swift
//  keykey
//
//  Created by lydio on 06.03.2021.
//

import Foundation


let app = App.create()

print("📘 Start all tests (Y):")
if readLine()?.uppercased() == "Y" {
    app.startAllTests()
}
var result = app.parseResultDirectory().last
print(result?.description ?? "")

if let failures = result?.actionResult?.testFailureSummaries, !failures.isEmpty {
    print("📙 Restart failed classes or methods (C/M):")
    var updateResult = true
    switch readLine()?.uppercased() {
        case "C":
            app.startFailedTestClasses(failures: failures)
        case "M":
            app.startFailedTestMethods(failures: failures)
        default:
            updateResult = false
    }
    if updateResult {
        result = app.parseResultDirectory().last
        print(result?.description ?? "")
    }
}

if let result = result {
    
    let resultJsonFilePath = "\(app.cacheDirectoryPath)/Result.json"
    Utils.writeToJsonFile(result, path: resultJsonFilePath)
    
    let resultHtmlFilePath = "\(app.cacheDirectoryPath)/Result.html"
    Utils.writeTextFile(result.html, path: resultHtmlFilePath)

    let resultTextFilePath = "\(app.cacheDirectoryPath)/Result.txt"
    Utils.writeTextFile(result.description, path: resultTextFilePath)

    print("📘 Select Dropbox, Google Drive (D/G) or enter email address:")
    switch readLine()?.uppercased() {
        case "D":
            Dropbox().share(localFilePath: resultHtmlFilePath)
        case "G":
            GoogleDrive().share(localFilePath: resultTextFilePath)
        case let email where email?.contains("@") == true:
            let gmail = GMail()
            gmail.emailTo = email!
            gmail.share(localFilePath: resultTextFilePath)
        default:
            break
    }
}

// /Users/lydio/Documents/Bitbucket/p2d/bd_ios/pd2.xcworkspace



