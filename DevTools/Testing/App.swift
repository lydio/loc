//
//  Settings.swift
//  DevTools
//
//  Created by lydio on 9/29/21.
//

import Foundation


class App:Codable {
    
    private var sourceDirectoryPath:String = ""
    private var xcodebuildCommand:String = ""
    private var testPlanName:String = ""
    private var simulator = Simulator()
    
    private static var cacheDirectoryPath:String = {
        let cache = Utils.shell("pwd") + "/Cache92921"
        let _ = Utils.createDirectory(at: cache)
        return cache
    }()
    var cacheDirectoryPath:String {
        return "\(type(of: self).cacheDirectoryPath)/\(sourceDirectoryPath)/\(testPlanName)"
    }
    private var resultDirectoryPath:String {
        return "\(cacheDirectoryPath)/Result"
    }
    
    func setup() {
        
        print("📘 Set *.xcworkspace or *.xcodeproj path:")
        var isDir = ObjCBool(false)
        guard let path = readLine(), path.count > 0, FileManager.default.fileExists(atPath: path, isDirectory: &isDir), isDir.boolValue else {
            print("📕 Path not found")
            fatalError()
        }
        var components = path.split(separator: "/")
        let lastComponent = components.removeLast()
        sourceDirectoryPath = "/" + components.joined(separator: "/")
        
        var xcodebuildCommand = "xcodebuild"
        
        if lastComponent.hasSuffix(".xcworkspace") {
            xcodebuildCommand += (" -workspace \"\(path)\"")
        } else if lastComponent.hasSuffix(".xcodeproj") {
            xcodebuildCommand += (" -project \"\(path)\"")
        }
        
        guard !xcodebuildCommand.isEmpty else {
            print("📕 Path not found")
            fatalError()
        }
       
        let listJson = Utils.shell(xcodebuildCommand + " -list -quiet -json")
        Utils.parseJson(from: listJson, recursive: true) { [unowned self] (key, value) in
            if key == "schemes", let items = value as? [String] {
                if let scheme = Utils.selectItem(items: items, name: "scheme")?.value {
                    xcodebuildCommand += " -scheme \"\(scheme)\""
                    self.xcodebuildCommand = xcodebuildCommand
                }
                return true
            }
            return false
        }
        
        guard !xcodebuildCommand.isEmpty else {
            print("📕 Scheme not selected")
            fatalError()
        }
        
        var items = [String]()
        let testPlansJson = Utils.shell(xcodebuildCommand + " -showTestPlans -json")
        Utils.parseJson(from: testPlansJson, recursive: true) { (key, value) in
            if key == "name", let item = value as? String {
                items.append(item)
            }
            return false
        }
        
        guard let plan = Utils.selectItem(items: items, name: "test plan")?.value else {
            print("📕 Test plan not selected")
            fatalError()
        }
        testPlanName = plan
        
        simulator.setup()
    }
    
    static func create() -> App {
        
        var app:App!
        let cachePath = App.cacheDirectoryPath
        let appCacheName = String(describing: self) + ".json"
        
        var cache = [App]()
        Utils.findContents(at: cachePath, recursive: true) { path, name, _, _ in
            if name == appCacheName, let app:App = Utils.readFromJsonFile(path: path) {
                cache.append(app)
            }
            return false
        }

        if let index =  Utils.selectItem(items: cache.map { "\($0.sourceDirectoryPath)#\($0.testPlanName)" }, name: "test plan")?.index {
            app = cache[index]
        }

        if app == nil {
            app = App()
            app.setup()
            Utils.writeToJsonFile(app, path: "\(app.cacheDirectoryPath)/\(appCacheName)")
        }
      
        return app
    }
   
    func startAllTests() {
        
        simulator.prepareForTesting()
        let _ = Utils.removeItem(at: resultDirectoryPath)
        
        var xcodebuildCommand = "\(xcodebuildCommand) -destination id=\(simulator.udid)"
        let _ = Utils.shell("\(xcodebuildCommand) build-for-testing -quiet")
        
        xcodebuildCommand += " -testPlan \"\(testPlanName)\""
        xcodebuildCommand += " -resultBundlePath \"\(resultDirectoryPath)/\(simulator.udid)\""
        xcodebuildCommand += " -parallel-testing-enabled \(simulator.numberOfSimulators > 1 ? 1 : 0)"
        xcodebuildCommand += " -parallel-testing-worker-count \(simulator.numberOfSimulators)"
        let _ = Utils.shell("\(xcodebuildCommand) test-without-building -quiet &")
    }
    
    private func startTests(_ tests:[String]) {
        
        simulator.prepareForTesting()
        let _ = Utils.removeItem(at: resultDirectoryPath)
        
        var xcodebuildCommand = "\(xcodebuildCommand) -destination id=\(simulator.udid)"
        let _ = Utils.shell("\(xcodebuildCommand) build-for-testing -quiet")
        
        let tests = tests
            .sorted()
            .map { "\"-only-testing:\($0)\"" }
            .joined(separator: " ")
        
        xcodebuildCommand += " \(tests)"
        xcodebuildCommand += " -testPlan \"\(testPlanName)\""
        xcodebuildCommand += " -resultBundlePath \"\(resultDirectoryPath)/\(simulator.udid)\""
        xcodebuildCommand += " -parallel-testing-enabled 0"
        let _ = Utils.shell("\(xcodebuildCommand) test-without-building -quiet &")
    }
    
    func startFailedTestMethods(failures: [TestFailureIssueSummary]) {
        
        let tests = failures.compactMap { ($0.producingTarget, $0.testCaseName) as? (String, String) }
            .map { $0.0 + "/" +  $0.1.replacingOccurrences(of: ".", with: "/").replacingOccurrences(of: "()", with: "")}
        
        startTests(tests)
    }
    
    func startFailedTestClasses(failures: [TestFailureIssueSummary]) {
        
        let tests = failures.compactMap { ($0.producingTarget, $0.testCaseName) as? (String, String) }
            .map { $0.0 + "/" + $0.1.replacingOccurrences(of: ".", with: "/").replacingOccurrences(of: "()", with: "") }
            .compactMap { $0.replacingOccurrences(of: "/test", with: "#").split(separator: "#").first }
            .map { String($0) }
            
        startTests(tests)
    }
    
    func parseResultDirectory() -> [Result] {
        
        let xcresultDirectoryPath = Utils.shell("find \"\(resultDirectoryPath)\" -type d  -name  *.xcresult -maxdepth 1")
        
        var results = [Result]()
        let resultJson = Utils.shell("xcrun xcresulttool get --path \"\(xcresultDirectoryPath)\" --format json")
        
        Utils.parseJson(from: resultJson, recursive: true) { (key, value) in
            if key == "actions", let dict = value as? [String:Any], let arr = dict["_values"] as? [[String:Any]] {
                results.append(contentsOf: arr.map { Result(dict: $0) })
                return true
            }
            return false
        }
        
        return results
    }
}
