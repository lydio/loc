//
//  main.swift
//  keykey
//
//  Created by lydio on 06.03.2021.
//

import Foundation

var sourceDirectoryPath = "Localization"
var destinationDirectoryPath = "Localization_out"

let argCount = CommandLine.argc
if argCount == 3 {
    sourceDirectoryPath = CommandLine.arguments[1]
    destinationDirectoryPath = CommandLine.arguments[2]
}

enum Utils {
    
    static func createDirectory(at path: String) -> Bool {
        do {
            let url = URL(fileURLWithPath: path)
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        } catch _ {
            return false
        }
        return true
    }
    
    static func removeContentsOfDirectory(at path: String) -> Bool {
        do {
            let url = URL(fileURLWithPath: path)
            let urls = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: [.isDirectoryKey], options: [])
            for u in urls {
                let attr = try u.resourceValues(forKeys:[.isDirectoryKey])
                if attr.isDirectory == true && !removeContentsOfDirectory(at: u.path) {
                    throw NSError()
                }
                try FileManager.default.removeItem(at: u)
            }
        } catch _ {
            return false
        }
        return true
    }
    
    static func findResources(ofType ext: String, at path: String) -> [URL] {
        
        var resources = [URL]()
        do {
            let url = URL(fileURLWithPath: path)
            let urls = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: [.isDirectoryKey, .isRegularFileKey], options: [])
            for u in urls {
                let attr = try u.resourceValues(forKeys:[.isDirectoryKey, .isRegularFileKey])
                if attr.isDirectory == true {
                    resources.append(contentsOf: findResources(ofType: ext, at: u.path))
                } else if attr.isRegularFile == true && u.pathExtension == ext {
                    resources.append(u)
                }
            }
        } catch _ {
            resources = []
        }
        return resources
    }
    
    static func replaceBase(to newBase:String, atPath path: inout String) -> Bool {
        
        let baseUrl = URL(fileURLWithPath: newBase)
        let url = URL(fileURLWithPath: path)
        
        let baseArr = baseUrl.pathComponents
        var arr = url.pathComponents
        
        if baseArr.count >= arr.count {
            return false
        }
       
        for i in 0..<baseArr.count {
            arr[i] = baseArr[i]
        }
        path = arr.joined(separator: "/").replacingOccurrences(of: "//", with: "/")
        return true
    }
    
    static func writeText(_ text: String, toFile path:String) -> Bool {
        do {
            let url = URL(fileURLWithPath: path)
            let _ = createDirectory(at: url.deletingLastPathComponent().path)
            try text.write(to: url, atomically: true, encoding: String.Encoding.utf8)
        } catch _ {
            return false
        }
        return true
    }
    
    static func findAllFormatSpecifiers(_ text:String) -> [String]? {
        
        guard text.contains("%") else {
            return nil
        }
        
        let text = text.replacingOccurrences(of: "% ", with: "pct ")
        guard text.contains("%") else {
            return nil
        }
        
        let p = "%(?:\\d+\\$)?[+-]?(?:[lh]{0,2})(?:[qLztj])?(?:[ 0]|'.{1})?\\d*(?:\\.\\d+)?[@dDiuUxXoOfeEgGcCsSpaAFn]"
        let regEx = try! NSRegularExpression(pattern: p, options: [])
        let result = regEx.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        
        return result.map {
            (text as NSString).substring(with: $0.range)
        }
    }
}


let _ = Utils.createDirectory(at: destinationDirectoryPath)
let _ = Utils.removeContentsOfDirectory(at: destinationDirectoryPath)

var values = [String]()
Utils.findResources(ofType: "strings", at: sourceDirectoryPath).forEach {
    
    if var dict = NSDictionary(contentsOf: $0) as? [String: String] {
        values.append(contentsOf: dict.values)
        
        for key in dict.keys {
            
            let value = dict[key]
            if let specifiers = Utils.findAllFormatSpecifiers(value!){
                dict[key] = key + "_" + specifiers.joined(separator: "_")
            } else {
                dict[key] = key
            }
        }
        let text = (dict as NSDictionary).descriptionInStringsFileFormat
        var path = $0.path
        let _ = Utils.replaceBase(to: destinationDirectoryPath, atPath: &path)
        let _ = Utils.writeText(text, toFile: path)
    }
}

values.sort()
let _ = Utils.writeText(values.joined(separator: "\n"), toFile: destinationDirectoryPath + "/all_values.txt")

var copies = [String:Int]()
values.forEach {
    if let i = copies[$0] {
        copies[$0] = i + 1
    } else {
        copies[$0] = 0
    }
}
copies = copies.filter {
    $0.value > 0
}
let total = copies.values.reduce(0, +)

values = (Array(copies).sorted { $0.value > $1.value }).map { "\($0.value) - \($0.key)" }
let _ = Utils.writeText(values.joined(separator: "\n"), toFile: destinationDirectoryPath + "/\(total)_copies.txt")

